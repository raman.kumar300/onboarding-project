FROM node:14-slim

WORKDIR /usr/src/app

COPY . .

RUN npm ci
RUN npm run build

ENV PORT 8080

EXPOSE 8080

CMD ["npm" , "start"]