"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Booking = exports.bookingSchema = void 0;
const mongoose_1 = require("mongoose");
var bookingSchema = new mongoose_1.Schema({
    status: { type: String, required: true },
    partner: { type: mongoose_1.Schema.Types.ObjectId },
    created_by: { type: mongoose_1.Schema.Types.ObjectId, required: true },
    start_time: { type: Date, required: true },
    from_hub: { type: mongoose_1.Schema.Types.ObjectId, required: true },
    to_hub: { type: mongoose_1.Schema.Types.ObjectId, required: true },
    created_at: { type: Date, required: true, default: Date.now },
    updated_at: { type: Date, required: true, default: Date.now },
});
exports.bookingSchema = bookingSchema;
const Booking = (0, mongoose_1.model)("Booking", bookingSchema);
exports.Booking = Booking;
