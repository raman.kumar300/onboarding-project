"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Hub = exports.hubSchema = void 0;
const mongoose_1 = require("mongoose");
var hubSchema = new mongoose_1.Schema({
    name: { type: String, unique: true, required: true, index: true },
    location: {
        type: mongoose_1.Schema.Types.Mixed,
        unique: true,
        required: true,
        index: true,
    },
    owner: { type: mongoose_1.Schema.Types.ObjectId, required: true },
});
exports.hubSchema = hubSchema;
// hubSchema.index({ name: 1, location: 1 }, { unique: true });
const Hub = (0, mongoose_1.model)("Hub", hubSchema);
exports.Hub = Hub;
