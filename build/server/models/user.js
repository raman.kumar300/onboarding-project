"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.User = exports.userSchema = void 0;
const mongoose_1 = require("mongoose");
var userSchema = new mongoose_1.Schema({
    name: { type: mongoose_1.Schema.Types.String, required: true },
    phone: { type: mongoose_1.Schema.Types.Number, unique: true, required: true },
    active: { type: mongoose_1.Schema.Types.Boolean, default: true },
    created_at: { type: mongoose_1.Schema.Types.Date, default: Date.now },
    updated_at: { type: mongoose_1.Schema.Types.Date, defualt: Date.now },
    type: { type: mongoose_1.Schema.Types.String, required: true },
    username: { type: String, required: true, unique: true },
    password: { type: String, required: true },
});
exports.userSchema = userSchema;
const User = (0, mongoose_1.model)("User", userSchema);
exports.User = User;
