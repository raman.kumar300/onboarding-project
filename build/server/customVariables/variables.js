"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Type = exports.Status = void 0;
const Status = {
    CREATED: "created",
    ASSIGNED: "assigned",
    CANCELLED: "cancelled",
    IN_PROGRESS: "in_progress",
    COMPLETED: "completed",
    EXPIRED: "expired",
};
exports.Status = Status;
const Type = {
    CLIENT: "Client",
    PARTNER: "Partner",
};
exports.Type = Type;
