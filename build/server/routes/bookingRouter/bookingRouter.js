"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const index_1 = __importDefault(require("../../controller/bookingController/index"));
const middleware_1 = __importDefault(require("../../middleware/middleware"));
const router = express_1.default.Router();
router.post("/create-booking", middleware_1.default.authenticateClient, index_1.default.createBooking);
router.get("/viewBookings", middleware_1.default.authenticatePartner, index_1.default.mostRecentBooking);
router.put("/applyBookings", middleware_1.default.authenticatePartner, index_1.default.applyBooking);
router.put("/updateBookings", middleware_1.default.authenticateClient, index_1.default.updateBooking);
router.put("/startTrip", middleware_1.default.authenticatePartner, index_1.default.startTrip);
router.put("/endTrip", middleware_1.default.authenticatePartner, index_1.default.endTrip);
exports.default = { router };
