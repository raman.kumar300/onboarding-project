"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const index_1 = __importDefault(require("../../controller/userController/index"));
const middleware_1 = __importDefault(require("../../middleware/middleware"));
const router = express_1.default.Router();
router.post("/createUsers", middleware_1.default.alreadySignIn, index_1.default.signUp);
router.post("/signIn", middleware_1.default.checkLoginAuthentication, index_1.default.login);
router.post("/signOut", middleware_1.default.authenticate, index_1.default.logout);
exports.default = { router };
