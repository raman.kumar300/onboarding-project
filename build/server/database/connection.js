"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.url = exports.connect = void 0;
const mongoose = require("mongoose");
const url = "mongodb://localhost:27017/Onboarding_Project";
exports.url = url;
const connect = async () => {
    try {
        await mongoose
            .connect(url, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            autoIndex: true,
        })
            .then(() => {
            console.log("connected to Database");
        });
    }
    catch (err) {
        console.log(err);
        process.exit(1);
    }
    return;
};
exports.connect = connect;
