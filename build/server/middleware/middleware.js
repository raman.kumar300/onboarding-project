"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const variables_1 = require("../customVariables/variables");
const createError = require("http-errors");
const authenticate = (req, res, next) => {
    try {
        if (req.session.isAuthenticate && req.session.user) {
            next();
        }
        else
            throw createError(401, "Authenticate First");
    }
    catch (err) {
        next(err);
    }
};
const checkLoginAuthentication = (req, res, next) => {
    try {
        if (req.session.isAuthenticate) {
            throw createError(400, "Already Logged In");
        }
        else
            next();
    }
    catch (err) {
        next(err);
    }
};
const authenticateClient = (req, res, next) => {
    try {
        if (req.session.isAuthenticate) {
            if (req.session.user.type === variables_1.Type.CLIENT) {
                next();
            }
            else {
                throw createError(403, "Access Only for Client Account");
            }
        }
        else {
            throw createError(401, "Authenticate First");
        }
    }
    catch (err) {
        next(err);
    }
};
const authenticatePartner = (req, res, next) => {
    try {
        if (req.session.isAuthenticate) {
            if (req.session.user.type === variables_1.Type.PARTNER) {
                next();
            }
            else {
                throw createError(403, "Access Only for Partner Account");
            }
        }
        else {
            throw createError(401, "Authenticate First");
        }
    }
    catch (err) {
        next(err);
    }
};
const alreadySignIn = (req, res, next) => {
    try {
        if (req.session.isAuthenticate) {
            throw createError(400, "Already Logged In");
        }
        else {
            next();
        }
    }
    catch (err) {
        next(err);
    }
};
exports.default = {
    authenticate,
    checkLoginAuthentication,
    authenticateClient,
    authenticatePartner,
    alreadySignIn,
};
