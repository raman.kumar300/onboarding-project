"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const variables_1 = require("../../customVariables/variables");
const booking_1 = require("../../models/booking");
const createError = require("http-errors");
const bull_1 = __importDefault(require("bull"));
const bookingQueue = new bull_1.default("BookingQueue", {
    redis: {
        port: 6379,
        host: "127.0.0.1",
    },
});
const ITEM_PER_PAGE = 3;
const createBooking = async (bookingData) => {
    try {
        const booking = new booking_1.Booking(bookingData);
        await booking.save();
        await bookingQueue.add(booking);
    }
    catch (err) {
        throw createError(400, err);
    }
};
const updateBooking = async (fieldsToBeUpdated, id) => {
    try {
        await booking_1.Booking.findByIdAndUpdate({ _id: id }, fieldsToBeUpdated);
    }
    catch (err) {
        throw createError(400, err);
    }
};
const listRecentBooking = async (to_hub, from_hub, page) => {
    try {
        const numberOfData = await booking_1.Booking.find({
            from_hub: from_hub ? from_hub : { $exists: true },
            to_hub: to_hub ? to_hub : { $exists: true },
            status: variables_1.Status.CREATED,
        }).count();
        const pageCount = Math.ceil(numberOfData / ITEM_PER_PAGE);
        if (page > pageCount) {
            throw createError(400, "Enter A Valid Page Number");
        }
        const data = await booking_1.Booking.find({
            from_hub: from_hub ? from_hub : { $exists: true },
            to_hub: to_hub ? to_hub : { $exists: true },
            status: variables_1.Status.CREATED,
        })
            .skip((page - 1) * ITEM_PER_PAGE)
            .limit(ITEM_PER_PAGE)
            .sort({
            updated_at: 1,
        });
        return { pageCount, data };
    }
    catch (err) {
        throw createError(400, err);
    }
};
const book = async (id, partner) => {
    try {
        const updateBookingData = {
            status: variables_1.Status.ASSIGNED,
            partner: partner,
            updated_at: new Date(),
        };
        await updateBooking(updateBookingData, id);
    }
    catch (err) {
        throw createError(400, err);
    }
};
exports.default = {
    createBooking,
    listRecentBooking,
    updateBooking,
    book,
};
