"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const variables_1 = require("../../customVariables/variables");
const controller_1 = __importDefault(require("./controller"));
const booking_1 = require("../../models/booking");
const createError = require("http-errors");
const createBooking = async (req, res, next) => {
    try {
        const { start_time, from_hub, to_hub } = req.body;
        if (!start_time || !from_hub || !to_hub) {
            throw createError(400, "Insufficient Data");
        }
        const parsed_start_time = new Date(start_time);
        const currentDate = new Date();
        if (currentDate > parsed_start_time) {
            throw createError(400, "Enter A Time That is not Passed");
        }
        const created_by = req.session.user;
        const bookingData = {
            start_time: parsed_start_time,
            from_hub,
            to_hub,
            created_by,
            status: variables_1.Status.CREATED,
        };
        await controller_1.default.createBooking(bookingData);
        res.status(200).json({ Message: "Booking Created Successfully" });
    }
    catch (err) {
        next(err);
    }
};
const updateBooking = async (req, res, next) => {
    try {
        const { from_hub, to_hub, start_time, status, id, partner } = req.body;
        const fieldsToBeUpdated = {
            updated_at: new Date(),
        };
        if (!id) {
            throw createError(400, "Missing Required Data");
        }
        if (from_hub) {
            fieldsToBeUpdated.from_hub = from_hub;
        }
        if (to_hub) {
            fieldsToBeUpdated.to_hub = to_hub;
        }
        if (start_time) {
            const parsed_start_time = new Date(start_time);
            const currentDate = new Date();
            if (currentDate > parsed_start_time) {
                throw createError(400, "Enter A Time That is not Passed");
            }
            fieldsToBeUpdated.start_time = parsed_start_time;
        }
        if (status) {
            fieldsToBeUpdated.status = status;
        }
        if (partner) {
            fieldsToBeUpdated.partner = partner;
        }
        await controller_1.default.updateBooking(fieldsToBeUpdated, id);
        res.status(200).json({ Message: "Update Booking Successful" });
    }
    catch (err) {
        next(err);
    }
};
const mostRecentBooking = async (req, res, next) => {
    try {
        var { from_hub, to_hub, page } = req.body;
        if (!page || page < 1) {
            throw createError(400, "Enter A Valid Page Number");
        }
        const bookingData = await controller_1.default.listRecentBooking(to_hub, from_hub, page);
        res.status(200).json(bookingData);
    }
    catch (err) {
        next(err);
    }
};
const applyBooking = async (req, res, next) => {
    try {
        const { id } = req.body;
        if (!id) {
            throw createError(400, "Missing Booking Id");
        }
        const user = req.session.user;
        const booking = await booking_1.Booking.findById(id);
        if (!booking) {
            throw createError(404, "No Booking With Given Id Found");
        }
        if (!booking.status || booking.status !== variables_1.Status.CREATED) {
            throw createError(400, "Booking Not Avalable ");
        }
        await controller_1.default.book(id, user);
        res.status(200).json({ Message: "Successfully Booked" });
    }
    catch (err) {
        next(err);
    }
};
const startTrip = async (req, res, next) => {
    try {
        const { id } = req.body;
        const user = req.session.user;
        if (!id) {
            throw createError(400, "Missing Booking Id");
        }
        const booking = await booking_1.Booking.findById(id);
        if (!booking) {
            throw createError(404, "No Booking With Given Id Found");
        }
        if (!booking.partner ||
            booking.partner.toString() !== user._id.toString()) {
            throw createError(400, "Booking Not Assigned To You ");
        }
        if (!booking.status || booking.status !== variables_1.Status.ASSIGNED) {
            throw createError(400, "Booking Not Avalable ");
        }
        const updatedBookingData = {
            status: variables_1.Status.IN_PROGRESS,
        };
        await controller_1.default.updateBooking(updatedBookingData, id);
        res.status(200).json({ Message: "Trip Started Successfully" });
    }
    catch (err) {
        next(err);
    }
};
const endTrip = async (req, res, next) => {
    try {
        const { id } = req.body;
        if (!id) {
            throw createError(400, "Missing Booking Id");
        }
        const user = req.session.user;
        const booking = await booking_1.Booking.findById(id);
        if (!booking) {
            throw createError(404, "No Booking With Given Id Found");
        }
        if (!booking.partner ||
            booking.partner.toString() !== user._id.toString()) {
            throw createError(400, "Booking Not Assigned To You ");
        }
        if (!booking.status || booking.status !== variables_1.Status.IN_PROGRESS) {
            throw createError(400, "Booking Not Under Progess");
        }
        const updatedBookingData = {
            status: variables_1.Status.COMPLETED,
        };
        await controller_1.default.updateBooking(updatedBookingData, id);
        res.status(200).json({ Message: "Trip Ended Successfully" });
    }
    catch (err) {
        next(err);
    }
};
exports.default = {
    mostRecentBooking,
    applyBooking,
    createBooking,
    updateBooking,
    endTrip,
    startTrip,
};
