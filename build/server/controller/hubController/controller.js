"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const hub_1 = require("../../models/hub");
const createError = require("http-errors");
const createHub = async (hubData) => {
    try {
        const newHub = new hub_1.Hub(hubData);
        await newHub.save();
    }
    catch (err) {
        throw createError(400, err);
    }
};
exports.default = { createHub };
