"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const controller_1 = __importDefault(require("./controller"));
const createError = require("http-errors");
const createHub = async (req, res, next) => {
    try {
        const { name, latitude, longitude } = req.body;
        if (!name || !latitude || !longitude) {
            throw createError(400, "Insufficient Data");
        }
        const location = {
            latitude,
            longitude,
        };
        const user = req.session.user;
        const hubData = {
            name,
            location,
            owner: user._id,
        };
        await controller_1.default.createHub(hubData);
        res.status(200).json({ message: "Hub Created Successsfully" });
    }
    catch (err) {
        next(err);
    }
};
exports.default = {
    createHub,
};
