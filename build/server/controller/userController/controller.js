"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const user_1 = require("../../models/user");
const createError = require("http-errors");
const bcrypt = require("bcryptjs");
const signUp = async (userInfo) => {
    try {
        const { username, password, name, phone, type } = userInfo;
        const user = new user_1.User({
            name: name,
            phone: phone,
            type: type,
            username: username,
            password: await bcrypt.hash(password, 12),
            active: true,
        });
        await user.save();
    }
    catch (err) {
        throw createError(err);
    }
};
const login = async (credentials) => {
    try {
        const { username, password } = credentials;
        const user = await user_1.User.findOne({ username: username });
        if (!user) {
            throw createError(400, "No User With Given User Name Found ");
        }
        const isPasswordMatch = await bcrypt.compare(password, user.password);
        if (!isPasswordMatch) {
            throw createError(401, "Enter Valid Password");
        }
        return user;
    }
    catch (err) {
        throw err;
    }
};
exports.default = {
    signUp,
    login,
};
