"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const variables_1 = require("../../customVariables/variables");
const controller_1 = __importDefault(require("./controller"));
const createError = require("http-errors");
const signUp = async (req, res, next) => {
    try {
        const { username, password, name, phone, type } = req.body;
        if (!username || !password || !name || !phone || !type) {
            throw createError(400, "Missing Required Fields");
        }
        if (type !== variables_1.Type.CLIENT && type !== variables_1.Type.PARTNER) {
            throw createError(400, "Type Can only be Client or Partner");
        }
        const userInfo = {
            username,
            password,
            name,
            phone,
            type,
        };
        await controller_1.default.signUp(userInfo);
        res.status(200).json({ AccountCreated: true });
    }
    catch (err) {
        next(err);
    }
};
const login = async (req, res, next) => {
    try {
        const { username, password } = req.body;
        if (!username || !password) {
            throw createError(400, "Missing Required Fields");
        }
        const credentials = {
            username,
            password,
        };
        const userData = await controller_1.default.login(credentials);
        if (!userData) {
            throw createError(404, "No User With Given User Name Found");
        }
        req.session.isAuthenticate = true;
        req.session.user = userData;
        res.status(200).json({ Message: "Login Successs" });
    }
    catch (err) {
        next(err);
    }
};
const logout = async (req, res, next) => {
    try {
        await req.session.destroy((err) => {
            if (err) {
                throw err;
            }
        });
        res.status(200).json({ Message: "Sign Out Successfull" });
    }
    catch (err) {
        next(err);
    }
};
exports.default = {
    signUp,
    login,
    logout,
};
