"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const bull_1 = __importDefault(require("bull"));
const bookingQueue = new bull_1.default("BookingQueue", {
    redis: {
        port: 6379,
        host: "127.0.0.1",
    },
});
setInterval(function () {
    bookingQueue.getJobCounts().then((res) => {
        console.log("result", res);
        if (res) {
            bookingQueue.process((job, done) => {
                console.log(job);
                done();
            });
        }
    });
    console.log("done");
}, 1000 * 5);
