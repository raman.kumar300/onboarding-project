"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const body_parser_1 = __importDefault(require("body-parser"));
const connection_1 = require("./server/database/connection");
const express_session_1 = __importDefault(require("express-session"));
const userRouter_1 = __importDefault(require("./server/routes/userRouter/userRouter"));
const bookingRouter_1 = __importDefault(require("./server/routes/bookingRouter/bookingRouter"));
const hubRouter_1 = __importDefault(require("./server/routes/hubRouter/hubRouter"));
const mongodbSession = require("connect-mongodb-session")(express_session_1.default);
const app = (0, express_1.default)();
const PORT = 3000;
const sessionStore = new mongodbSession({
    uri: connection_1.url,
    collection: "userSession",
});
(0, connection_1.connect)();
app.use(body_parser_1.default.urlencoded({
    extended: true,
}));
app.use(body_parser_1.default.json());
app.use((0, express_session_1.default)({
    secret: "sessionSecretToken",
    resave: false,
    saveUninitialized: false,
    store: sessionStore,
}));
app.use("/user/", userRouter_1.default.router);
app.use("/booking/", bookingRouter_1.default.router);
app.use("/hub/", hubRouter_1.default.router);
app.use((err, req, res, next) => {
    res.status(err.status || 500);
    res.send({
        Error: {
            Status: err.status || 500,
            Message: err.message,
        },
    });
    next();
});
app.listen(PORT, () => {
    console.log(`Server is running on ${PORT}`);
});
