import { Request, Response, NextFunction } from "express";
import { Type } from "../customVariables/variables";
const createError = require("http-errors");

const authenticate = (req: Request, res: Response, next: NextFunction) => {
  try {
    if (req.session.isAuthenticate && req.session.user) {
      next();
    } else throw createError(401, "Authenticate First");
  } catch (err) {
    next(err);
  }
};

const checkLoginAuthentication = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    if (req.session.isAuthenticate) {
      throw createError(400, "Already Logged In");
    } else next();
  } catch (err) {
    next(err);
  }
};

const authenticateClient = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    if (req.session.isAuthenticate) {
      if (req.session.user.type === Type.CLIENT) {
        next();
      } else {
        throw createError(403, "Access Only for Client Account");
      }
    } else {
      throw createError(401, "Authenticate First");
    }
  } catch (err) {
    next(err);
  }
};

const authenticatePartner = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  try {
    if (req.session.isAuthenticate) {
      if (req.session.user.type === Type.PARTNER) {
        next();
      } else {
        throw createError(403, "Access Only for Partner Account");
      }
    } else {
      throw createError(401, "Authenticate First");
    }
  } catch (err) {
    next(err);
  }
};

const alreadySignIn = (req: Request, res: Response, next: NextFunction) => {
  try {
    if (req.session.isAuthenticate) {
      throw createError(400, "Already Logged In");
    } else {
      next();
    }
  } catch (err) {
    next(err);
  }
};

export default {
  authenticate,
  checkLoginAuthentication,
  authenticateClient,
  authenticatePartner,
  alreadySignIn,
};
