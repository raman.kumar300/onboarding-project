import { hubInterface } from "../../customVariables/interface";
import { Hub } from "../../models/hub";
const createError = require("http-errors");

const createHub = async (hubData: hubInterface) => {
  try {
    const newHub = new Hub(hubData);
    await newHub.save();
  } catch (err) {
    throw createError(400, err);
  }
};
export default { createHub };
