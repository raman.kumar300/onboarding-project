import { Request, Response, NextFunction } from "express";
import { hubInterface, Location } from "../../customVariables/interface";
import hubController from "./controller";

const createError = require("http-errors");

const createHub = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { name, latitude, longitude } = req.body;

    if (!name || !latitude || !longitude) {
      throw createError(400, "Insufficient Data");
    }

    const location: Location = {
      latitude,
      longitude,
    };

    const user = req.session.user;

    const hubData: hubInterface = {
      name,
      location,
      owner: user._id,
    };

    await hubController.createHub(hubData);
    res.status(200).json({ message: "Hub Created Successsfully" });
  } catch (err) {
    next(err);
  }
};

export default {
  createHub,
};
