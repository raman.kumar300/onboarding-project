import { Request, Response, NextFunction } from "express";
import { type } from "os";
import {
  loginCredentials,
  userInterface,
} from "../../customVariables/interface";
import { Type } from "../../customVariables/variables";
import accountController from "./controller";
const createError = require("http-errors");

const signUp = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { username, password, name, phone, type } = req.body;

    if (!username || !password || !name || !phone || !type) {
      throw createError(400, "Missing Required Fields");
    }

    if (type !== Type.CLIENT && type !== Type.PARTNER) {
      throw createError(400, "Type Can only be Client or Partner");
    }

    const userInfo: userInterface = {
      username,
      password,
      name,
      phone,
      type,
    };
    await accountController.signUp(userInfo);
    res.status(200).json({ AccountCreated: true });
  } catch (err) {
    next(err);
  }
};

const login = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { username, password } = req.body;

    if (!username || !password) {
      throw createError(400, "Missing Required Fields");
    }

    const credentials: loginCredentials = {
      username,
      password,
    };

    const userData = await accountController.login(credentials);

    if (!userData) {
      throw createError(404, "No User With Given User Name Found");
    }

    req.session.isAuthenticate = true;
    req.session.user = userData;

    res.status(200).json({ Message: "Login Successs" });
  } catch (err) {
    next(err);
  }
};

const logout = async (req: Request, res: Response, next: NextFunction) => {
  try {
    await req.session.destroy((err) => {
      if (err) {
        throw err;
      }
    });
    res.status(200).json({ Message: "Sign Out Successfull" });
  } catch (err) {
    next(err);
  }
};

export default {
  signUp,
  login,
  logout,
};
