import { createTracing } from "trace_events";
import {
  loginCredentials,
  userInterface,
} from "../../customVariables/interface";
import { User } from "../../models/user";
const createError = require("http-errors");

const bcrypt = require("bcryptjs");

const signUp = async (userInfo: userInterface) => {
  try {
    const { username, password, name, phone, type } = userInfo;
    const user = new User({
      name: name,
      phone: phone,
      type: type,
      username: username,
      password: await bcrypt.hash(password, 12),
      active: true,
    });
    await user.save();
  } catch (err) {
    throw createError(err);
  }
};

const login = async (credentials: loginCredentials) => {
  try {
    const { username, password } = credentials;

    const user = await User.findOne({ username: username });

    if (!user) {
      throw createError(400, "No User With Given User Name Found ");
    }

    const isPasswordMatch = await bcrypt.compare(password, user.password);

    if (!isPasswordMatch) {
      throw createError(401, "Enter Valid Password");
    }

    return user;
  } catch (err) {
    throw err;
  }
};

export default {
  signUp,
  login,
};
