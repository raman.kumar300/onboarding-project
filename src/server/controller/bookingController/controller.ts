import { Status } from "../../customVariables/variables";
import {
  bookingInterface,
  FieldsToBeUpdated,
  hubInterface,
} from "../../customVariables/interface";
import { Booking } from "../../models/booking";
const createError = require("http-errors");
import Queue from "bull";

const bookingQueue = new Queue("BookingQueue", {
  redis: {
    port: 6379,
    host: "127.0.0.1",
  },
});

const ITEM_PER_PAGE = 3;

const createBooking = async (bookingData: bookingInterface) => {
  try {
    const booking = new Booking(bookingData);
    await booking.save();
    await bookingQueue.add(booking);
  } catch (err) {
    throw createError(400, err);
  }
};

const updateBooking = async (
  fieldsToBeUpdated: FieldsToBeUpdated,
  id: String
) => {
  try {
    await Booking.findByIdAndUpdate({ _id: id }, fieldsToBeUpdated);
  } catch (err) {
    throw createError(400, err);
  }
};

const listRecentBooking = async (
  to_hub: hubInterface | null | undefined,
  from_hub: hubInterface | null | undefined,
  page: number
) => {
  try {
    const numberOfData = await Booking.find({
      from_hub: from_hub ? from_hub : { $exists: true },
      to_hub: to_hub ? to_hub : { $exists: true },
      status: Status.CREATED,
    }).count();

    const pageCount = Math.ceil(numberOfData / ITEM_PER_PAGE);

    if (page > pageCount) {
      throw createError(400, "Enter A Valid Page Number");
    }

    const data = await Booking.find({
      from_hub: from_hub ? from_hub : { $exists: true },
      to_hub: to_hub ? to_hub : { $exists: true },
      status: Status.CREATED,
    })
      .skip((page - 1) * ITEM_PER_PAGE)
      .limit(ITEM_PER_PAGE)
      .sort({
        updated_at: 1,
      });

    return { pageCount, data };
  } catch (err) {
    throw createError(400, err);
  }
};

const book = async (id: String, partner: String) => {
  try {
    const updateBookingData: FieldsToBeUpdated = {
      status: Status.ASSIGNED,
      partner: partner,
      updated_at: new Date(),
    };

    await updateBooking(updateBookingData, id);
  } catch (err) {
    throw createError(400, err);
  }
};

export default {
  createBooking,
  listRecentBooking,
  updateBooking,
  book,
};
