import express, { Router, Request, Response, NextFunction } from "express";
import controller from "../../controller/bookingController/index";
import middleware from "../../middleware/middleware";

const router: Router = express.Router();

router.post(
  "/create-booking",
  middleware.authenticateClient,
  controller.createBooking
);
router.get(
  "/viewBookings",
  middleware.authenticatePartner,
  controller.mostRecentBooking
);

router.put(
  "/applyBookings",
  middleware.authenticatePartner,
  controller.applyBooking
);

router.put(
  "/updateBookings",
  middleware.authenticateClient,
  controller.updateBooking
);

router.put("/startTrip", middleware.authenticatePartner, controller.startTrip);

router.put("/endTrip", middleware.authenticatePartner, controller.endTrip);

export default { router };
