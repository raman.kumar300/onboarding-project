import express, { Router } from "express";
import controller from "../../controller/hubController/index";
import middleware from "../../middleware/middleware";

const router: Router = express.Router();

router.post("/createHub", middleware.authenticate, controller.createHub);

export default { router };
