import express, { Router } from "express";
import controller from "../../controller/userController/index";
import middleware from "../../middleware/middleware";

const router: Router = express.Router();

router.post("/createUsers", middleware.alreadySignIn, controller.signUp);

router.post("/signIn", middleware.checkLoginAuthentication, controller.login);

router.post("/signOut", middleware.authenticate, controller.logout);

export default { router };
