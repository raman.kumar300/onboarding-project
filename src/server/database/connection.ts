const mongoose = require("mongoose");

const url: string = "mongodb://localhost:27017/Onboarding_Project";

const connect = async (): Promise<void> => {
  try {
    await mongoose
      .connect(url, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        autoIndex: true,
      })
      .then(() => {
        console.log("connected to Database");
      });
  } catch (err) {
    console.log(err);
    process.exit(1);
  }
  return;
};

export { connect, url };
