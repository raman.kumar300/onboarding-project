import Queue from "bull";

const bookingQueue = new Queue("BookingQueue", {
  redis: {
    port: 6379,
    host: "127.0.0.1",
  },
});

setInterval(function () {
  bookingQueue.getJobCounts().then((res) => {
    console.log("result", res);
    if (res) {
      bookingQueue.process((job, done) => {
        console.log(job);
        done();
      });
    }
  });
  console.log("done");
}, 1000 * 5);
