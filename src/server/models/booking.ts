import { Schema, model } from "mongoose";
import { bookingModelInterface } from "../customVariables/interface";

var bookingSchema = new Schema<bookingModelInterface>({
  status: { type: String, required: true },
  partner: { type: Schema.Types.ObjectId },
  created_by: { type: Schema.Types.ObjectId, required: true },
  start_time: { type: Date, required: true },
  from_hub: { type: Schema.Types.ObjectId, required: true },
  to_hub: { type: Schema.Types.ObjectId, required: true },
  created_at: { type: Date, required: true, default: Date.now },
  updated_at: { type: Date, required: true, default: Date.now },
});

const Booking = model<bookingModelInterface>("Booking", bookingSchema);

export { bookingSchema, Booking };
