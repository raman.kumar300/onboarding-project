import { Schema, model } from "mongoose";

var hubSchema = new Schema({
  name: { type: String, unique: true, required: true, index: true },
  location: {
    type: Schema.Types.Mixed,
    unique: true,
    required: true,
    index: true,
  },
  owner: { type: Schema.Types.ObjectId, required: true },
});

// hubSchema.index({ name: 1, location: 1 }, { unique: true });

const Hub = model("Hub", hubSchema);

export { hubSchema, Hub };
