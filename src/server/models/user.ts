import { Schema, model } from "mongoose";

var userSchema = new Schema({
  name: { type: Schema.Types.String, required: true },
  phone: { type: Schema.Types.Number, unique: true, required: true },
  active: { type: Schema.Types.Boolean, default: true },
  created_at: { type: Schema.Types.Date, default: Date.now },
  updated_at: { type: Schema.Types.Date, defualt: Date.now },
  type: { type: Schema.Types.String, required: true },
  username: { type: String, required: true, unique: true },
  password: { type: String, required: true },
});

const User = model("User", userSchema);

export { userSchema, User };
