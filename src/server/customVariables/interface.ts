import mongoose from "mongoose";

export interface userModelInterface extends mongoose.Document {
  name: String;
  phone: number;
  active?: Boolean;
  created_at?: Date;
  updated_at?: Date;
  type: String;
  username: String;
  password: String;
}
export interface userInterface {
  name: String;
  phone: number;
  active?: Boolean;
  created_at?: Date;
  updated_at?: Date;
  type: String;
  username: String;
  password: String;
}

export interface hubModelInterface extends mongoose.Document {
  name: String;
  latitude: String;
  longitude: String;
  owner: String;
}

export interface Location {
  latitude: String;
  longitude: String;
}

export interface hubInterface {
  name: String;
  location: Location;
  owner: String;
}

export interface bookingModelInterface extends mongoose.Document {
  status?: String;
  partner?: String;
  created_by: String;
  start_time: Date;
  from_hub: String;
  to_hub: String;
  created_at?: Date;
  updated_at?: Date;
}

export interface bookingInterface {
  status?: String;
  partner?: String;
  created_by: String;
  start_time: Date;
  from_hub: String;
  to_hub: String;
  created_at?: Date;
  updated_at?: Date;
}

export interface loginCredentials {
  username: String;
  password: String;
}

export interface FieldsToBeUpdated {
  start_time?: Date;
  from_hub?: String;
  to_hub?: String;
  status?: String;
  partner?: String;
  updated_at?: Date;
}
