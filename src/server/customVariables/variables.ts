const Status = {
  CREATED: "created",
  ASSIGNED: "assigned",
  CANCELLED: "cancelled",
  IN_PROGRESS: "in_progress",
  COMPLETED: "completed",
  EXPIRED: "expired",
};
const Type = {
  CLIENT: "Client",
  PARTNER: "Partner",
};

export { Status, Type };
