import express, { Application, NextFunction } from "express";
import bodyparser from "body-parser";
import { connect, url } from "./server/database/connection";
import session from "express-session";
import userRouter from "./server/routes/userRouter/userRouter";
import bookingRouter from "./server/routes/bookingRouter/bookingRouter";
import hubRouter from "./server/routes/hubRouter/hubRouter";

// WORK NEED TO BE DONE MARKING BOOKING AS EXPIRED
//require('./server/backgroundtask/index')

declare module "express-session" {
  interface SessionData {
    isAuthenticate: Boolean;
    user: any;
  }
}

const mongodbSession = require("connect-mongodb-session")(session);
const app: Application = express();

const PORT: Number = 3000;
const sessionStore = new mongodbSession({
  uri: url,
  collection: "userSession",
});

connect();
app.use(
  bodyparser.urlencoded({
    extended: true,
  })
);
app.use(bodyparser.json());
app.use(
  session({
    secret: "sessionSecretToken",
    resave: false,
    saveUninitialized: false,
    store: sessionStore,
  })
);

app.use("/user/", userRouter.router);
app.use("/booking/", bookingRouter.router);
app.use("/hub/", hubRouter.router);

app.use(
  (
    err: { status: any; message: any },
    req: any,
    res: {
      status: (arg0: any) => void;
      send: (arg0: { Error: { Status: any; Message: any } }) => void;
    },
    next: any
  ) => {
    res.status(err.status || 500);
    res.send({
      Error: {
        Status: err.status || 500,
        Message: err.message,
      },
    });
    next();
  }
);

app.listen(PORT, () => {
  console.log(`Server is running on ${PORT}`);
});
